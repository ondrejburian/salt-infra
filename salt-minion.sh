#!/bin/bash
salt-ssh --priv /opt/deploy-key -i -r '*' 'sudo wget -O - https://repo.saltstack.com/py3/ubuntu/18.04/amd64/latest/SALTSTACK-GPG-KEY.pub | sudo apt-key add -'
salt-ssh --priv /opt/deploy-key -i -r '*' "echo 'deb http://repo.saltstack.com/py3/ubuntu/18.04/amd64/latest bionic main' | sudo tee /etc/apt/sources.list.d/saltstack.list"
salt-ssh --priv /opt/deploy-key -i -r '*' 'sudo apt update; sudo apt install -y salt-minion'
