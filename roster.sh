cat /etc/hosts | awk -F' ' '$2~/^jenkins/' | awk -F' ' ' NF {print $2": " $1} ' | cut -d '-' -f 5 > /tmp/roster

#Remove default roster file
sudo rm /etc/salt/roster

CONFIG="/etc/salt/roster"
if [ ! -e $CONFIG ]; then
  while read x ; do
    server=$(echo $x | awk -F ':' '{print $1}' | sed -e 's/\ //g')
    ip=$(echo $x | awk -F ':' '{print $2}' | sed -e 's/\ //g')
    echo "$server:" >> /etc/salt/roster
    echo "  host: $ip" >> /etc/salt/roster
    echo "  user: ubuntu" >> /etc/salt/roster
    echo "  sudo: True" >> /etc/salt/roster
  done < /tmp/roster
fi

